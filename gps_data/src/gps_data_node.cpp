#include "ros/ros.h"
#include "sensor_msgs/NavSatFix.h"
#include <std_msgs/String.h>
#include <iostream>
#include <fstream>

using namespace std;
ofstream gpsfile("gpsdata.txt", std::ofstream::out);

void gpscall(const sensor_msgs::NavSatFixConstPtr& gpsmsg)
{
	gpsfile << gpsmsg->header.stamp << "\n";
	gpsfile << gpsmsg->header.seq;
	gpsfile << gpsmsg->header.frame_id  << "\n";
	gpsfile << gpsmsg->latitude << "\n" ;
	gpsfile << gpsmsg->longitude << "\n" ;
	gpsfile << gpsmsg->altitude << "\n";
}

int main(int argc, char **argv)
{
	ros::init(argc, argv, "gps_data_node");
	ros::NodeHandle n;
	ros::Subscriber odom_data = n.subscribe("/fix", 1000,gpscall);
	ros::spin();
	gpsfile.close();
	return 0;
}
