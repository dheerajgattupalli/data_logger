#include "ros/ros.h"
#include "nav_msgs/Odometry.h"
#include <std_msgs/String.h>
//#include <simplelist/Size.h>
#include <geometry_msgs/Pose.h>
#include "sensor_msgs/Imu.h"

#include <iostream>
#include <fstream>

using namespace std;
ofstream imufile;

void imucall(const sensor_msgs::Imu imumsg){
	imufile << imumsg.header.stamp << "\n";
	imufile << imumsg.header.seq << "\n" ;
	imufile << imumsg.header.frame_id << "\n"; 
	imufile << imumsg.orientation.x << "\n" ;
	imufile << imumsg.orientation.y << "\n" ;
	imufile << imumsg.orientation.z << "\n" ;
	imufile << imumsg.orientation.w<<"\n";
	imufile << imumsg.angular_velocity.x << "\n"; 
	imufile << imumsg.angular_velocity.y << "\n" ;
	imufile << imumsg.angular_velocity.z << "\n";
	imufile << imumsg.linear_acceleration.x << "\n"; 
	imufile << imumsg.linear_acceleration.y << "\n" ;
	imufile << imumsg.linear_acceleration.z<<"\n";
}





int main(int argc, char **argv)
{
	ros::init(argc, argv, "imu_data_node");
	ros::NodeHandle n;
	imufile.open("imudata.txt");
	ros::Subscriber imu_data = n.subscribe("/imu/data", 1000,imucall);
	ros::spin();
	imufile.close();
	return 0;
}

