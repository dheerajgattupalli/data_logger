#include "ros/ros.h"
#include "nav_msgs/Odometry.h"
#include <std_msgs/String.h>
#include <sensor_msgs/Imu.h>
#include <sensor_msgs/NavSatFix.h>
#include <iostream>
#include <fstream>
#include <stdlib.h> 
#include <stdio.h>
#include <string>
using namespace std;
fstream imudatafile;
fstream odomdatafile;
fstream gpsdatafile;

nav_msgs::Odometry odommsg;
sensor_msgs::Imu imumsg;
sensor_msgs::NavSatFix gpsmsg;

//int imucount = 1;
//int gpscount = 1;
//int odomcount = 1;
ros::Publisher imu_pub,gps_pub,odom_pub; 
double imutime,odomtime,gpstime;
double str2double(string str)
{
	long double value1,value2,value;
	int flag=0;
	int i=0;
	value1=0,value2=0;
	while(str[i]!='.'){
		int c=str[i]-48;
		value1=value1*10+c;
		i++;
	}
	i=str.length()-1;
	while(str[i]!='.'){
		int c=str[i]-48;
		value2=value2/10+c;
		i--;
	}
	value2=value2/10;
	i=str.length()-1;
	value=value1+value2;
	return value;
}

void imusend()
{
	/*
	   std_msgs/Header header
	   uint32 seq
	   time stamp
	   string frame_id
	   geometry_msgs/Quaternion orientation
	   float64 x
	   float64 y
	   float64 z
	   float64 w
	   geometry_msgs/Vector3 angular_velocity
	   float64 x
	   float64 y
	   float64 z
	   geometry_msgs/Vector3 linear_acceleration
	   float64 x
	   float64 y
	   float64 z
	 */

	string x;
	double y;
	string frame;
	imudatafile.open("imudata.txt");
	imumsg.header.stamp = ros::Time(imutime);
	system("sed -i '1d'  imudata.txt");
	getline(imudatafile,x);
	y=str2double(x);
	imumsg.header.seq = (uint)y;
	system("sed -i '1d'  imudata.txt");
	getline(imudatafile,frame);
	imumsg.header.frame_id = frame ; 
	system("sed -i '1d'  imudata.txt");
	getline(imudatafile,x);
	y=str2double(x);
	imumsg.orientation.x = y;
	system("sed -i '1d'  imudata.txt");
	getline(imudatafile,x);
	y=str2double(x);
	imumsg.orientation.y = y;
	system("sed -i '1d'  imudata.txt");
	getline(imudatafile,x);
	y=str2double(x);
	imumsg.orientation.z= y;
	system("sed -i '1d'  imudata.txt");
	getline(imudatafile,x);
	y=str2double(x);
	imumsg.orientation.w = y;
	system("sed -i '1d'  imudata.txt");
	getline(imudatafile,x);
	y=str2double(x);
	imumsg.angular_velocity.x = y;
	system("sed -i '1d'  imudata.txt");
	getline(imudatafile,x);
	y=str2double(x);
	imumsg.angular_velocity.y = y;
	system("sed -i '1d'  imudata.txt");
	getline(imudatafile,x);
	y=str2double(x);
	imumsg.angular_velocity.z = y;
	system("sed -i '1d'  imudata.txt");
	getline(imudatafile,x);
	y=str2double(x);
	imumsg.linear_acceleration.x  = y;
	system("sed -i '1d'  imudata.txt");
	getline(imudatafile,x);
	y=str2double(x);
	imumsg.linear_acceleration.y = y;
	system("sed -i '1d'  imudata.txt");
	getline(imudatafile,x);
	y=str2double(x);
	imumsg.linear_acceleration.z = y;
	system("sed -i '1d'  imudata.txt");
	imudatafile.close();
	imu_pub.publish(imumsg);
}

void odomsend()
{
	/*	std_msgs/Header header
		uint32 seq
		time stamp
		string frame_id
		string child_frame_id
		geometry_msgs/PoseWithCovariance pose
		geometry_msgs/Pose pose
		geometry_msgs/Point position
		float64 x
		float64 y
		float64 z
		geometry_msgs/Quaternion orientation
		float64 x
		float64 y
		float64 z
		float64 w
		float64[36] covariance
		geometry_msgs/TwistWithCovariance twist
		geometry_msgs/Twist twist
		geometry_msgs/Vector3 linear
		float64 x
		float64 y
		float64 z
		geometry_msgs/Vector3 angular
		float64 x
		float64 y
		float64 z
		float64[36] covariance
	 */
	string x;
	double y;
	string frame;
	odomdatafile.open("odomdata.txt");
	odommsg.header.stamp = ros::Time(odomtime);
	system("sed -i '1d'  odomdata.txt");
	getline(odomdatafile,x);
	y=str2double(x);
	odommsg.header.seq = (uint)y;
	system("sed -i '1d'  odomdata.txt");
	getline(odomdatafile,frame);
	odommsg.header.frame_id = frame ; 
	system("sed -i '1d'  odomdata.txt");
	getline(odomdatafile,frame);
	odommsg.child_frame_id = frame;
	system("sed -i '1d'  odomdata.txt");
	getline(odomdatafile,x);
	y=str2double(x);
	odommsg.pose.pose.position.x = y;
	system("sed -i '1d'  odomdata.txt");
	getline(odomdatafile,x);
	y=str2double(x);
	odommsg.pose.pose.position.y = y;
	system("sed -i '1d'  odomdata.txt");
	getline(odomdatafile,x);
	y=str2double(x);
	odommsg.pose.pose.position.z = y;
	system("sed -i '1d'  odomdata.txt");
	getline(odomdatafile,x);
	y=str2double(x);
	odommsg.pose.pose.orientation.x = y;
	system("sed -i '1d'  odomdata.txt");
	getline(odomdatafile,x);
	y=str2double(x);
	odommsg.pose.pose.orientation.y = y;
	system("sed -i '1d'  odomdata.txt");
	getline(odomdatafile,x);
	y=str2double(x);
	odommsg.pose.pose.orientation.z = y;
	system("sed -i '1d'  odomdata.txt");
	getline(odomdatafile,x);
	y=str2double(x);
	odommsg.pose.pose.orientation.w = y;
	system("sed -i '1d'  odomdata.txt");
	getline(odomdatafile,x);
	y=str2double(x);
	odommsg.twist.twist.linear.x = y;
	system("sed -i '1d'  odomdata.txt");
	getline(odomdatafile,x);
	y=str2double(x);
	odommsg.twist.twist.linear.y = y;
	system("sed -i '1d'  odomdata.txt");
	getline(odomdatafile,x);
	y=str2double(x);
	odommsg.twist.twist.linear.z = y;
	system("sed -i '1d'  odomdata.txt");
	getline(odomdatafile,x);
	y=str2double(x);
	odommsg.twist.twist.angular.x = y;
	system("sed -i '1d'  odomdata.txt");
	getline(odomdatafile,x);
	y=str2double(x);
	odommsg.twist.twist.angular.y = y;
	system("sed -i '1d'  odomdata.txt");
	getline(odomdatafile,x);
	y=str2double(x);
	odommsg.twist.twist.angular.z = y;
	system("sed -i '1d'  odomdata.txt");
	odomdatafile.close();
	odom_pub.publish(odommsg);
}
void gpssend()
{
	/*
	   std_msgs/Header header
	   uint32 seq
	   time stamp
	   string frame_id
	   float64 latitude
	   float64 longitude
	   float64 altitude

	 */
	sensor_msgs::NavSatFix gpsmsg;
	string x;
	double y;
	string frame;
	gpsdatafile.open("gpsdata.txt");
	gpsmsg.header.stamp = ros::Time(gpstime);
	system("sed -i '1d'  gpsdata.txt");
	getline(gpsdatafile,x);
	y=str2double(x);
	gpsmsg.header.seq = (uint)y;
	system("sed -i '1d'  gpsdata.txt");
	getline(gpsdatafile,frame);
	gpsmsg.header.frame_id = frame ; 
	system("sed -i '1d'  gpsdata.txt");
	getline(gpsdatafile,x);
	y=str2double(x);
	gpsmsg.latitude = y;
	system("sed -i '1d'  gpsdata.txt");
	getline(gpsdatafile,x);
	y=str2double(x);
	gpsmsg.longitude = y;
	system("sed -i '1d'  gpsdata.txt");
	getline(gpsdatafile,x);
	y=str2double(x);
	gpsmsg.altitude = y;
	gpsdatafile.close();
	gps_pub.publish(gpsmsg);
}

int main(int argc, char** argv)
{
	ros::init(argc, argv, "send_data_node");
	ros::NodeHandle node1;
	ros::NodeHandle node2;
	ros::NodeHandle node3;
	double y,leasttime;
	string x;
	int flag=0,flag1=0,flag2=0,flag3=0;
	imu_pub = node1.advertise<sensor_msgs::Imu>("/imu/data", 1000);
	odom_pub = node2.advertise<nav_msgs::Odometry>("/encoder", 1000);
	gps_pub = node3.advertise<sensor_msgs::NavSatFix>("/fix", 1000);
	while(flag1==0 || flag2==0 || flag3==0){
		imudatafile.open("imudata.txt");
		odomdatafile.open("odomdata.txt");
		gpsdatafile.open("gpsdata.txt");
		if(flag1==0){
			getline(imudatafile,x);
			if(x.empty()){
				if(flag2==1)
					flag=6;
				else if(flag3==1)
					flag=5;
				else
					flag=1;
				flag1=1;
			}
			imutime=str2double(x);
			imudatafile.close();
		}
		if(flag2==0){
			getline(odomdatafile,x);
			//cout<<x.length();
			if(x.empty()){
				if(flag1==1)
					flag=6;
				else if(flag3==1)
					flag=4;
				else
					flag=2;
				flag2=1;
			}
			odomtime=str2double(x);
			odomdatafile.close();
		}
		if(flag3==0){
			getline(gpsdatafile,x);
			if(x.empty()){
				if(flag1==1)
					flag=5;
				else if(flag2==1)
					flag=4;
				else
					flag=3;
				flag3=1;
			}
			gpstime=str2double(x);
			gpsdatafile.close();
		}
		if(flag1==1 && flag2==1 && flag3==1)
			flag=7;
		switch(flag)
		{
			case 0 :
				if(odomtime<gpstime){
					if(imutime<odomtime){
						imusend();
					}
					else{
						odomsend();
					}
				}
				else{
					if(imutime<gpstime){
						imusend();
					}
					else{
						gpssend();
					}
				}
				break;
			case 1 :
				if(odomtime<gpstime){
					odomsend();
				}
				else{
					gpssend();
				}
				break;
			case 2 :
				if(imutime<gpstime){
					imusend();
				}
				else{
					gpssend();
				}
				break;
			case 3 :
				if(odomtime<imutime){
					odomsend();
				}
				else{
					imusend();
				}
				break;	
			case 4 :
				imusend();
				break;
			case 5 :
				odomsend();
				break;
			case 6 :
				gpssend();
				break;
			default :
				break;
		}
	}
	return 0;
}