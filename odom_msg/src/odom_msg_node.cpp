#include "ros/ros.h"
#include "nav_msgs/Odometry.h"
#include <std_msgs/String.h>
#include <iostream>
#include <fstream>

using namespace std;
ofstream odomfile("odomdata.txt", std::ofstream::out);

void odomcall(const nav_msgs::OdometryConstPtr& odommsg)
{
	odomfile << odommsg->header.stamp << "\n";
	odomfile << odommsg->header.seq << "\n" ;
	odomfile << odommsg->header.frame_id  << "\n";
	odomfile << odommsg->child_frame_id << "\n";
	odomfile << odommsg->pose.pose.position.x << "\n" ;
	odomfile << odommsg->pose.pose.position.y << "\n" ;
	odomfile << odommsg->pose.pose.position.z << "\n";
	odomfile << odommsg->pose.pose.orientation.x << "\n" ;
	odomfile << odommsg->pose.pose.orientation.y << "\n" ; 
	odomfile << odommsg->pose.pose.orientation.z << "\n";
	odomfile << odommsg->pose.pose.orientation.w << "\n";
	odomfile << odommsg->twist.twist.linear.x << "\n" ;
	odomfile << odommsg->twist.twist.linear.y << "\n" ;
	odomfile << odommsg->twist.twist.linear.z << "\n";
	odomfile << odommsg->twist.twist.angular.x << "\n" ;
	odomfile << odommsg->twist.twist.angular.y << "\n" ;
	odomfile << odommsg->twist.twist.angular.z << "\n";
}

		
		
int main(int argc, char **argv)
{
	ros::init(argc, argv, "odom_data_node");
	ros::NodeHandle n;
	ros::Subscriber odom_data = n.subscribe("/encoder", 1000,odomcall);
	ros::spin();
	odomfile.close();
	return 0;
}
